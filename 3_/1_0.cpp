#include <unordered_map>
#include <iostream>

class phone {
    std::unordered_map<unsigned, phone*> contacts;
    unsigned number;
    public:
    phone(unsigned num) :number(num) {}
    unsigned get_num(){return number;}
    phone* call(unsigned num) {
        return contacts[num];
    }
    void add_contact(unsigned num, phone* phone) {
        contacts[num] = phone;
    }
};

int main(void) {
    phone p(123);
    phone p2(456);

    p.add_contact(69, &p2);

    std::cout << p.call(69)->get_num() << std::endl;
}
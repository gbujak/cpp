#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

int main(){
    string line; 
    vector<string> vec;
    getline(cin, line);
    while (line != "end") {
        vec.push_back(line);
        getline(cin, line);
    }
    ofstream plik;
    plik.open("out.txt");
    for (string s : vec) {
        plik << s << endl;
    }
    plik.close();
}
#include <iostream>
#include <vector>
#include <cstdlib>

using namespace std;

class Stack {
    vector<int> vec;
    unsigned int size;
    public:
    Stack() {
        size = 0;
    } 
    void push(int x) {
        vec.push_back(x);
        size++;
    }
    int pop() {
        if (size == 0) abort();
        int ret = vec[size--];
        vec.pop_back(); 
        return ret;
    }
};

int main (){
    Stack stack;
    for (int i = 0; i < 100; i++) {
        stack.push(i);
    }
    for (int i = 0; i < 100; i++) {
        cout << stack.pop() << ", ";
    }
    cout << endl;
}
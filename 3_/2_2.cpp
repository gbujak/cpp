#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main() {
    string line;
    ifstream plik;
    plik.open("out.txt");
    getline(plik, line);
    if (!plik.eof()) cout << line << endl;
    while (!plik.eof()) {
        getline(plik, line);
        cout << line << endl;
    }
}
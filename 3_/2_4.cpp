#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <iostream>
#include <cstring>

using namespace std;

class person {
    // tabela znakÃ³w w celu Åatwiejszego zapisania do pliku binarnego
    static const int len = 50;
    char name[len];
    char surname[len];
    unsigned age;
public:
    person(string name, string surname, unsigned age) 
        : age(age) {
            strncpy(this->name, name.c_str(), len);
            strncpy(this->surname, surname.c_str(), len);
        }
    person(){}
    string describe(){
        stringstream ss;
        ss << name << ", " << surname << ", " << age;
        return ss.str();
    }
};

int main() {
    {
        vector<person> vec;
        vec.push_back(person("Adam", "Nowak", 24));
        vec.push_back(person("Jam", "Kowalaki", 45));
        vec.push_back(person("Andrzej", "Syn cieÅli", 24));

        ofstream f;
        f.open("people.bin", ios::binary);
        for (person x : vec)
            f.write(reinterpret_cast<char*>(&x), sizeof(x));
        vec.clear();
        f.close();
    }

    vector<person> vec;
    ifstream f2;
    f2.open("people.bin", ios::binary);

    for (int i = 0; i < 3; i++) {
        person x;
        f2.read(reinterpret_cast<char*>(&x), sizeof(x));
        vec.push_back(x);
    }

    f2.close();

    for (person x : vec)
        cout << x.describe() << '\n';

    remove("people.bin");
}
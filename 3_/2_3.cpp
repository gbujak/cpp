#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iostream>
#include <vector>

int main(void) {
    std::srand(std::time(nullptr));
    std::ofstream f;
    f.open("num_out.bin", std::ios::binary);
    for(int i = 0; i < 10; i++) {
        int x = rand() % 11;
        f.write(reinterpret_cast<char*>(&x), sizeof(x));
    }
    f.close();
    std::ifstream f2;
    f2.open("num_out.bin", std::ios::binary);
    std::vector<int> vec;
    for(int i = 0; i < 10; i++) {
        int x;
        f2.read(reinterpret_cast<char*>(&x), sizeof(x));
        std::cout << x << ", ";
        vec.push_back(x);
    }
    std::cout << std::endl;
    f2.close();

    int hist[11] = {0};
    for(int x : vec)
        hist[x]++;
    std::cout << "histogram: ";
    for(int i = 0; i < 11; i++)
        std::cout << hist[i] << ", ";
    std::cout << '\n';

    double average = 0;
    for (int x : vec) average += (double) x;
    average /= 10;
    std::cout << "average: " << average << '\n';

    remove("num_out.bin");
}
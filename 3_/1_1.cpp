#include <string>
#include <string.h>
#include <vector>
#include <iostream>

using namespace std;

class Osoba {
    int waga;
    public:
    int wzrost;
    string imie, nazwisko, pesel;
    public:
    Osoba(string imie, string nazwisko) {
        this->imie = imie;
        this->nazwisko = nazwisko;
    }
    void set_name(string name) {
        this->imie = name;
    }
    void set_surname(string nazw) {
        this->nazwisko = nazw;
    }
    void set_pesel(string pesel) {
        this->pesel = pesel;
    }
    void set_waga(int waga) {this->waga = waga;}
    int get_waga() {return waga;}
};

Osoba find_name(vector<Osoba> vec, string name) {
    for (vector<Osoba>::iterator i = vec.begin(); i != vec.end(); i++) {
        if (i->imie == name) return *i;
    }
    abort();
}

Osoba find_surname(vector<Osoba> vec, string surname) {
    for (vector<Osoba>::iterator i = vec.begin(); i != vec.end(); i++) {
        if (i->nazwisko == surname) return *i;
    }
    abort();
}

int main () {
    vector<Osoba> vec;
    vec.push_back(Osoba("jam", "kowalaki"));
    vec.push_back(Osoba("jan", "kowalski"));
    vec.push_back(Osoba("andrzej", "syn cieÅli"));

    cout << find_name(vec, "andrzej").nazwisko << endl;
    cout << find_surname(vec, "syn cieÅli").imie << endl;
}

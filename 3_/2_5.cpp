#include <string>
#include <iostream>
#include <ctime>
#include <cstdlib>
#include <vector>

using namespace std;

int main() {
    srand(time(nullptr));
    int tab[10];
    string s;
    for (int i = 0; i < 10; i++) tab[i] = rand() % 51;
    for (int i = 0; i < 10; i++) {
        s += to_string(tab[i]);
        s += " ";
    }
    cout << s << '\n';
    vector<int> vec;
    int j = 0;
    for (int i = 0; i < s.length(); i++) {
        if (s[i] == ' ') {
            vec.push_back(stoi(s.substr(j, i - j)));
            j = i;
        }
    }
    for (int x : vec)
        cout << x << " ";
    cout << endl;
}
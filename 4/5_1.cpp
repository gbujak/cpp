#include <iostream>
#include <string>

#define PI 3.14

using namespace std;

class roslina {
    protected:
    unsigned wysokosc;
    string kolor;
    public:
    roslina(){}
    roslina(unsigned w, string k) 
    :wysokosc(w), kolor(k) {}
    string getkolor() {return kolor;}
    unsigned getwys()   {return wysokosc;}
};

class drzewo : public roslina {
    protected:
    unsigned liscie;
    string owoce;
    public:
    drzewo(unsigned w, string k, string o, unsigned l)
    :owoce(o), liscie(l) {
        kolor = k;
        wysokosc = w;
    }
    string getowoce() {return owoce;}
    unsigned getliscie() {return liscie;}
};

class kolo {
    protected:
    double promien;
    public:
    kolo(){}
    void set_promien(double p) {
        promien = p;
    }
    double pole_kola() {
        return promien * promien * PI;
    }   
};

class odcinek {
    protected:
    double dlugosc;
    public:
    void set_dlugosc(double d) {
        dlugosc = d;
    }
    double get_dlugosc() {return dlugosc;}
};

class walec : public kolo, public odcinek {
    public:
    walec(double promien, double wysokosc) {
        set_promien(promien);
        set_dlugosc(wysokosc);
    }
    double pole() {
        return kolo::pole_kola() * get_dlugosc();
    }
};

int main(void) {
    drzewo d(100, "braz", "orzech", 2000);

    roslina* r_ptr = (roslina*) &d;

    cout << r_ptr->getwys() << endl;
    cout << r_ptr->getkolor() << endl;

    drzewo* d_ptr = (drzewo*) r_ptr;

    cout << d_ptr->getliscie() << endl;
    cout << d_ptr->getowoce() << endl;

    roslina* tabela[2];
    tabela[0] = new roslina(120, "zielen");
    tabela[1] = new drzewo(120, "zielen", "jablko", 5000);

    cout << tabela[0]->getkolor() << endl;
    cout << ((drzewo*) tabela[1])->getowoce() << endl;

    delete tabela[0]; delete tabela[1];

    walec w(10, 20);
    cout << w.get_dlugosc() << endl;
    cout << w.pole_kola() << endl;
    cout << w.pole() << endl;
}
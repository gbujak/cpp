

| Programowanie w C1 | Instrukcja nr. 5, 6 |
| ------------------ | ------------------- |
| Grzegorz Bujak     | Grupa 1ID11B        |
| 13 zaplanowane     | 13 wykonane         |

### Instrukcja nr. 5

```cpp
#include <iostream>
#include <string>

#define PI 3.14

using namespace std;

class roslina {
    protected:
    unsigned wysokosc;
    string kolor;
    public:
    roslina(){}
    roslina(unsigned w, string k) 
    :wysokosc(w), kolor(k) {}
    string getkolor() {return kolor;}
    unsigned getwys()   {return wysokosc;}
};

class drzewo : public roslina {
    protected:
    unsigned liscie;
    string owoce;
    public:
    drzewo(unsigned w, string k, string o, unsigned l)
    :owoce(o), liscie(l) {
        kolor = k;
        wysokosc = w;
    }
    string getowoce() {return owoce;}
    unsigned getliscie() {return liscie;}
};

class kolo {
    protected:
    double promien;
    public:
    kolo(){}
    void set_promien(double p) {
        promien = p;
    }
    double pole_kola() {
        return promien * promien * PI;
    }   
};

class odcinek {
    protected:
    double dlugosc;
    public:
    void set_dlugosc(double d) {
        dlugosc = d;
    }
    double get_dlugosc() {return dlugosc;}
};

class walec : public kolo, public odcinek {
    public:
    walec(double promien, double wysokosc) {
        set_promien(promien);
        set_dlugosc(wysokosc);
    }
    double pole() {
        return kolo::pole_kola() * get_dlugosc();
    }
};

int main(void) {
    drzewo d(100, "braz", "orzech", 2000);

    roslina* r_ptr = (roslina*) &d;

    cout << r_ptr->getwys() << endl;
    cout << r_ptr->getkolor() << endl;

    drzewo* d_ptr = (drzewo*) r_ptr;

    cout << d_ptr->getliscie() << endl;
    cout << d_ptr->getowoce() << endl;

    roslina* tabela[2];
    tabela[0] = new roslina(120, "zielen");
    tabela[1] = new drzewo(120, "zielen", "jablko", 5000);

    cout << tabela[0]->getkolor() << endl;
    cout << ((drzewo*) tabela[1])->getowoce() << endl;

    delete tabela[0]; delete tabela[1];

    walec w(10, 20);
    cout << w.get_dlugosc() << endl;
    cout << w.pole_kola() << endl;
    cout << w.pole() << endl;
}
```



### Instrukcja nr. 6

##### Zadania 1-4

```cpp
#include <iostream>
#include <string>

using namespace std;

class kwadrat {
    protected:
    double a;
    public:
    kwadrat(){};
    kwadrat(double a) :a(a) {}
    virtual double pole() {
        return a*a;
    }
};

class prostokat : public kwadrat {
    protected:
    double b;
    public:
    prostokat(double a, double b) : b(b) {
        this->a = a;
    }
    virtual double pole() {
        return a*b;
    }
};

int main (void) {
    cout << "tabela wskaźników:" << endl;

    kwadrat* tabela[2];
    tabela[0] = new kwadrat(5);
    tabela[1] = new prostokat(5, 10);
    cout << tabela[0]->pole() << endl;
    cout << tabela[1]->pole() << endl;
    delete tabela[0]; delete tabela[1];

    cout << "tabela obiektów" << endl; 

    kwadrat tabela2[2];
    tabela2[0] = kwadrat(5);
    tabela2[1] = prostokat(5, 10);
    cout << tabela2[0].pole() << endl;
    cout << tabela2[1].pole() << endl;

    // SEG FAULT \/
    // cout << ((prostokat*) &tabela[0])->pole() << endl;
} 
```

##### Zadania 5-6

```cpp
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class zwierze {
    public:
    virtual void daj_glos() = 0;
};

class pies : public zwierze {
    public:
    virtual void daj_glos() {
        cout << "Hau!\n";
    }
};

class kot : public zwierze {
    public:
    virtual void daj_glos() {
        cout << "Miau!\n";
    }
};

int main() {
    vector<zwierze*> vec;
    for (int i = 0; i < 10; i++)
        if (i % 2) vec.push_back(new pies);
              else vec.push_back(new kot);

    for (zwierze* i : vec) i->daj_glos();

    for (zwierze* i : vec) delete i;
}
```



Zadania nie sprawiły mi problemu, wykonując je zgłębiłem programowanie obiektowe w języku C++.

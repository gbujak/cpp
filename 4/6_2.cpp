#include <iostream>
#include <string>
#include <vector>

using namespace std;

class zwierze {
    public:
    virtual void daj_glos() = 0;
};

class pies : public zwierze {
    public:
    virtual void daj_glos() {
        cout << "Hau!\n";
    }
};

class kot : public zwierze {
    public:
    virtual void daj_glos() {
        cout << "Miau!\n";
    }
};

int main() {
    vector<zwierze*> vec;
    for (int i = 0; i < 10; i++)
        if (i % 2) vec.push_back(new pies);
        else vec.push_back(new kot);

    for (zwierze* i : vec) i->daj_glos();

    for (zwierze* i : vec) delete i;
}
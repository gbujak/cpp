#include <iostream>
#include <string>

using namespace std;

class kwadrat {
    protected:
    double a;
    public:
    kwadrat(){};
    kwadrat(double a) :a(a) {}
    virtual double pole() {
        return a*a;
    }
};

class prostokat : public kwadrat {
    protected:
    double b;
    public:
    prostokat(double a, double b) : b(b) {
        this->a = a;
    }
    virtual double pole() {
        return a*b;
    }
};

int main (void) {
    cout << "tabela wskaźników:" << endl;

    kwadrat* tabela[2];
    tabela[0] = new kwadrat(5);
    tabela[1] = new prostokat(5, 10);
    cout << tabela[0]->pole() << endl;
    cout << tabela[1]->pole() << endl;
    delete tabela[0]; delete tabela[1];

    cout << "tabela obiektów" << endl; 

    kwadrat tabela2[2];
    tabela2[0] = kwadrat(5);
    tabela2[1] = prostokat(5, 10);
    cout << tabela2[0].pole() << endl;
    cout << tabela2[1].pole() << endl;

    // SEG FAULT \/
    // cout << ((prostokat*) &tabela[0])->pole() << endl;

} 
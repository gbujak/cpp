#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

int main(void) {
    int min, max = 0;
    int tmp;
    int number = 0;
    cout << "ilość podawanych liczb? ";
    cin >> number;
    for (int i = 0; i < number; i++) {
        cin >> tmp;
        if (tmp > max) max = tmp;
        if (tmp < min) min = tmp;
    }
    if (min == max)
        cout << "Podane liczby są równe!" << endl;
    ofstream f;
    f.open("out.txt");
    f << "najmniejsza: " << min << endl;
    f << "największa:  " << max << endl;
    f.close();
}
#include <vector>
#include <iostream>

using namespace std;

int main(void) {
    vector<int> vec;
    for (int i = 1; i < 1000; i *= 5)
        vec.push_back(i);
    
    for (vector<int>::iterator i = vec.begin(); i != vec.end(); i++)
        cout << *i << ", ";
    cout << endl;

    // LUB

    for (auto i = vec.begin(); i != vec.end(); i++)
        cout << *i << ", ";
    cout << endl;

    // LUB

    for (int i : vec) 
        cout << i << ", ";
    cout << endl;

    vec.pop_back();

    for (int i : vec) 
        cout << i << ", ";
    cout << endl;

    cout << "rozmiar wektora: "
         << vec.size() << endl;
}
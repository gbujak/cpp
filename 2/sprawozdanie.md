| Programowanie w C1 | Instrukcja nr. 2 |
| ------------------ | ---------------- |
| Grzegorz Bujak     | Grupa 1ID11B     |
| 11 zaplanowane     | 11 wykonane      |



### Zadania 1 - 7

#### Struktura plików

* main.cpp

* fib.cpp

* fib.h

* field.cpp

* field.h

* silnia.cpp

* silnia.h



#### Plik makefile

Plik służy do kompilowania programu, którego działanie spełnia założenia poleceń 1-7

```makefile
output: main.o field.o fib.o silnia.o
	g++ main.o field.o fib.o silnia.o -o output

field.o: field.cpp field.h
	g++ -c field.cpp

fib.o: fib.cpp fib.h
	g++ -c fib.cpp

silnia.o: silnia.cpp silnia.h
	g++ -c silnia.cpp

clean:
	rm *.o output
```

#### main.cpp

```cpp
#include <iostream>
#include "field.h"
#include "fib.h"
#include "silnia.h"

using namespace std;

int main (void) {
    cout <<
    "Pole trójkąta a = 2, h = 5 -> " <<
    triangle(2, 5) << endl <<
    "Pole trapezu (a, b, h) = (2, 5, 10) -> " <<
    trapeze(2, 5, 10) << endl << endl <<
    "20-ty element ciągu fibonacciego -> " <<
    fib(20) << endl << endl <<
    "silnia z 10 -> " <<
    silnia(10) << endl;
}
```

#### fib.h i fib.cpp

```cpp
#ifndef FIB_H
#define FIB_H

int fib(int);

#endif
```

```cpp
#include "fib.h"

int fib (int n) {
    if (n == 0) return 0;
    if (n == 1 || n == 2) return 1;
    return fib(n-1) + fib(n-2); 
}
```

#### field.h i field.cpp

```cpp
#ifndef FIELD_H
#define FIELD_H

double circle   (double);
double trapeze  (double, double, double);
double triangle (double, double);

#endif
```

```cpp
#include "field.h"
#include <cmath>

double circle (double radius) {
    return M_PI * pow(radius, 2.0);
}

double trapeze (double a, double b, double h) {
    return 0.5 * (a + b) * h;
}

double triangle (double a, double h) {
    return 0.5 * a * h;
}
```

#### silnia.h i silnia.cpp

```cpp
#ifndef SILNIA_H
#define SILNIA_H

int silnia(int);

#endif
```

```cpp
#include "silnia.h"

int silnia(int x) {
    if (x == 0) return 1;
    else return x * silnia(x-1);
}
```



### Zadanie 8

W zadaniu należało utrwalić wiedzę o przeciążaniu funkcji.

```cpp
#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

#define LEN 100

float average(float a, float b) {
    return (a + b) * 0.5;
}

float average(float a, float b, float c) {
    return (a + b + c) / 3.0;
}

float average(float arr[]) {
    float sum = 0;
    for (int i = 0; i < LEN; i++) sum += arr[i];
    return sum / (float) LEN;
}

float average(float arr[], int beg, int end) {
    float sum = 0;
    for (int i = beg; i < end; i++) sum += arr[beg];
    return sum / (float) (end - beg);
}

int main(void) {
    srand(time(NULL));
    float arr[LEN];
    for (int i = 0; i < LEN; i++)
        arr[i] = (float) (rand() % 101);

    cout <<
    "Średnia z 5 i 2 -> " <<
    average(5, 2) << endl <<
    "Średnia z 4, 6, 10 -> " <<
    average(4, 6, 10) << endl <<
    "Średnia losowej tabeli -> " <<
    average(arr) << endl <<
    "Średnia tabeli, gdy x w <10, 50) -> " <<
    average(arr, 10, 50) << endl;
}
```



### Zadanie 9

Celem było utrwalenie wiadomości o metodach klasy ```std::string```.

```cpp
#include <string>
#include <iostream>

using namespace std;

int main (void) {
    string str; 
    getline(cin,str);

    cout << "string początkowy: "; 
    cout << str << endl; 

    str.push_back('X'); 
  
    cout << "string po operacji push_back: "; 
    cout << str << endl; 
  
    str.pop_back(); 
  
    cout << "string po operacji pop_back: "; 
    cout << str << endl; 

    cout << "string z dodanym string'iem: "; 
    cout << str + " kota" << endl; 

    { // blok kodu zwolni iteratory beg i end z pamięci
        auto beg = str.begin();
        auto end = str.end();

        while (beg != end) {
            cout << *beg << " ";
            beg ++;
        }
    }
    cout << endl;
}
```



### Zadania 10 - 11

Zadania miały na celu utrwalenie wiadomości o iteratorach w języku C++. Dodatkowo, użyłem słowa ```auto``` (automatyczne ustalenie typu zmiennej przez kompilator) oraz  dodanej w C++11 pętli  "range-based for loop".

```cpp
#include <string>
#include <vector>
#include <iostream>

using namespace std;

string findstr (vector<string> &vec, string query) {
    for (auto i : vec) {
    // To samo, co for (auto i = vec.begin(); i != vec.end(); i++)
        if (i.find(query) != string::npos) {
            return i;
        }
    }
    return "";
}

int main (void) {
    vector<string> vec;
    {
        string str;
        for (int i = 0; i < 4; i++) {
            cin >> str;
            vec.push_back(str);
        }
    }
    
    string query;
    cout << "co wyszukać? ";
    cin >> query;

    cout << query << " znajduje się w wektorze w string'u \"" <<
    findstr(vec, query) << "\"" << endl;

    for (auto beg = vec.begin(); beg != vec.end(); beg++)
        cout << *beg << ", ";
    cout << endl;
}
```



Zadania wykonałem bez problemu, poznałem funkcje języka C++, które pomagają w pisaniu większych programów.





























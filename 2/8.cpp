#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

#define LEN 100

float average(float a, float b) {
    return (a + b) * 0.5;
}

float average(float a, float b, float c) {
    return (a + b + c) / 3.0;
}

float average(float arr[]) {
    float sum = 0;
    for (int i = 0; i < LEN; i++) sum += arr[i];
    return sum / (float) LEN;
}

float average(float arr[], int beg, int end) {
    float sum = 0;
    for (int i = beg; i < end; i++) sum += arr[beg];
    return sum / (float) (end - beg);
}

int main(void) {
    srand(time(NULL));
    float arr[LEN];
    for (int i = 0; i < LEN; i++)
        arr[i] = (float) (rand() % 101);

    cout <<
    "Średnia z 5 i 2 -> " <<
    average(5, 2) << endl <<
    "Średnia z 4, 6, 10 -> " <<
    average(4, 6, 10) << endl <<
    "Średnia losowej tabeli -> " <<
    average(arr) << endl <<
    "Średnia tabeli, gdy x w <10, 50) -> " <<
    average(arr, 10, 50) << endl;
}
#include <string>
#include <iostream>

using namespace std;

int main (void) {
    string str; 
    getline(cin,str);

    cout << "string początkowy: "; 
    cout << str << endl; 

    str.push_back('X'); 
  
    cout << "string po operacji push_back: "; 
    cout << str << endl; 
  
    str.pop_back(); 
  
    cout << "string po operacji pop_back: "; 
    cout << str << endl; 

    cout << "string z dodanym string'iem: "; 
    cout << str + " kota" << endl; 

    { // blok kodu zwolni iteratory beg i end z pamięci
        auto beg = str.begin();
        auto end = str.end();

        while (beg != end) {
            cout << *beg << " ";
            beg ++;
        }
    }
    cout << endl;
}
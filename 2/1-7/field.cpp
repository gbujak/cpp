#include "field.h"
#include <cmath>

double circle (double radius) {
    return M_PI * pow(radius, 2.0);
}

double trapeze (double a, double b, double h) {
    return 0.5 * (a + b) * h;
}

double triangle (double a, double h) {
    return 0.5 * a * h;
}
#ifndef FIELD_H
#define FIELD_H

double circle   (double);
double trapeze  (double, double, double);
double triangle (double, double);

#endif
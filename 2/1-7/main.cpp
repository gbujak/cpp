#include <iostream>
#include "field.h"
#include "fib.h"
#include "silnia.h"

using namespace std;

int main (void) {
    cout <<
    "Pole trójkąta a = 2, h = 5 -> " <<
    triangle(2, 5) << endl <<
    "Pole trapezu (a, b, h) = (2, 5, 10) -> " <<
    trapeze(2, 5, 10) << endl << endl <<
    "20-ty element ciągu fibonacciego -> " <<
    fib(20) << endl << endl <<
    "silnia z 10 -> " <<
    silnia(10) << endl;
}


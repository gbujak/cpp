#include <string>
#include <vector>
#include <iostream>

using namespace std;

string findstr (vector<string> &vec, string query) {
    for (auto i : vec) {
    // To samo, co for (auto i = vec.begin(); i != vec.end(); i++)
        if (i.find(query) != string::npos) {
            return i;
        }
    }
    return "";
}

int main (void) {
    vector<string> vec;
    {
        string str;
        for (int i = 0; i < 4; i++) {
            cin >> str;
            vec.push_back(str);
        }
    }
    
    string query;
    cout << "co wyszukać? ";
    cin >> query;

    cout << query << " znajduje się w wektorze w string'u \"" <<
    findstr(vec, query) << "\"" << endl;

    for (auto beg = vec.begin(); beg != vec.end(); beg++)
        cout << *beg << ", ";
    cout << endl;
}